# Prerequesites

## Connect the local machine to the cluster:

```bash
# create brand new wireguard keys
wg genkey | tee privatekey | wg pubkey > publickey

# Find a non-assigned IP in the 10.5.0.0/24 range:
ssh root@minio-packet.freedesktop.org \
      "kubectl get -A peers.kilo.squat.ai -o json | \
       jq '[.items[]] | sort_by(.metadata.name) | .[] | {\"name\": .metadata.name, \"ip\": .spec.allowedIPs[]}'"
IP=10.5.0.10  # <- there is a high chance you need to replace
              #    that value with the IP you just chose

PEER_NAME=test-peer  # <- replace with the name you want

# add your new peer over ssh:
cat <<EOF | ssh root@minio-packet.freedesktop.org \
      kubectl apply -f -
apiVersion: kilo.squat.ai/v1alpha1
kind: Peer
metadata:
  name: $PEER_NAME
spec:
  allowedIPs:
  - ${IP}/32
  publicKey: $(cat publickey)
  persistentKeepalive: 10
EOF

# create the local wireguard config
cat <<EOF > fdo-minio.conf
[Interface]
Address = ${IP}/32
PrivateKey = $(cat privatekey)
MTU = 1350

[Peer]
AllowedIPs = 10.42.0.0/24, 10.99.237.129/32, 10.4.0.1/32
Endpoint = minio-packet.freedesktop.org:51820
PersistentKeepalive = 10
PublicKey = +Itf1F4BbjT/6755ZfDyUMpoFvXRAaUu0vky1JAYTFQ=
EOF
wg-quick up ./fdo-minio.conf

# fetch the k3s config file
mkdir -p ~/.kube/
scp root@10.4.0.1:/etc/rancher/k3s/k3s.yaml ~/.kube/
sed -i -e 's/127.0.0.1/10.4.0.1/' ~/.kube/k3s.yaml

KUBECONFIG=~/.kube/k3s.yaml kubectl get -A pods
```

# Prep work

We need to install helmfile and do some configs once we can connect
to the cluster over wireguard.

```bash
# install latest helm 3 (https://github.com/helm/helm/releases)
curl -LO https://get.helm.sh/helm-v3.3.1-linux-amd64.tar.gz
tar xvfz helm-v3.3.1-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/helm
rm -rf helm-v3.3.1-linux-amd64.tar.gz linux-amd64

# install helm diff plugin
helm plugin install https://github.com/databus23/helm-diff --version master

# install helmfile (https://github.com/roboll/helmfile/releases)
curl -LO https://github.com/roboll/helmfile/releases/download/v0.129.3/helmfile_linux_386
chmod +x helmfile_linux_386
mv helmfile_linux_386 /usr/loca/bin/helmfile

# download and install mc if you do not have it
curl -LO https://dl.min.io/client/mc/release/linux-amd64/mc
chmod +x mc
mv mc ~/bin
rehash

# enable autocompletion in your shell
mc --autocompletion
```

# Bootstrap effort (to be done once, when reinstalling):

To deploy from scratch the current setup, we need to do it with a
2-stage bootstrap:
- first, we need to install cert-manager so that our kubernetes
  cluster knows about the CRD `issuers.cert-manager.io` from this
  deployment
- then we can start deploying MinIO and request a Let's Encrypt
  certificate for it. *But*, if we do not set up a self-signed
  certificate first, the ACME challenge will fail as NGINX will
  redirect the http request to an https one, and the certificate
  is non existent, which leads to a failure.

So once the 2 bootstraps stages are done, cert-manager gracefully
requests the Let's Encrypt certificate and we are good to go.

```bash

# first create random secrets
openssl rand -base64 20 > values/minio/fdo-minio/accessKey
openssl rand -base64 40 > values/minio/fdo-minio/secretKey

# then bootstrap to install cert-manager, nginx
KUBECONFIG=~/.kube/k3s.yaml helmfile -e bootstrap apply

# second bootstrap to install minio with the issuers
KUBECONFIG=~/.kube/k3s.yaml helmfile -e bootstrap2 apply

# add our minio server to mc
mc config host add fdo https://minio-packet.freedesktop.org \
    $(cat values/minio/fdo-minio/accessKey) \
    $(cat values/minio/fdo-minio/secretKey)

# test
mc ls fdo/

# initialized our buckets
# create the 2 buckets we are using
mc mb fdo/git-cache
mc mb fdo/artifacts

# set both buckets as public (read)
mc policy set download fdo/git-cache
mc policy set download fdo/artifacts
```

# Fetch current MinIO config (before applying an update)
```bash
KUBECONFIG=~/.kube/k3s.yaml kubectl -n minio \
    get secrets fdo-minio -o json | \
    jq -r '.data.accesskey' | \
    base64 -d > values/minio/fdo-minio/accessKey
KUBECONFIG=~/.kube/k3s.yaml kubectl -n minio \
    get secrets fdo-minio -o json | \
    jq -r '.data.secretkey' | \
    base64 -d > values/minio/fdo-minio/secretKey

# add our minio server to mc
mc config host add fdo https://minio-packet.freedesktop.org \
    $(cat values/minio/fdo-minio/accessKey) \
    $(cat values/minio/fdo-minio/secretKey)
```

# Do an update
```bash
KUBECONFIG=~/.kube/k3s.yaml helmfile -i apply
```

Note: if you already have downloaded the latest charts, use:

```bash
KUBECONFIG=~/.kube/k3s.yaml helmfile -i apply --skip-deps
```
