# How to move a project to a new shard, keeping git deduplication

## move a single project manually

In the rails console

```bash
kubectl -n gitlab exec -ti $(kubectl -n gitlab get pod -l app=toolbox -o name | grep -v backup) -- /srv/gitlab/bin/rails console
```

```ruby
# get a project
user_project = Project.find_by_full_path('bentiss/test')

# show current shard
user_project.repository_storage

# initiate the move
storage_move = user_project.repository_storage_moves.build({:source_storage_name=>user_project.repository_storage, :destination_storage_name=>"gitaly-1"})
storage_move.schedule

# check if the move finished
# from https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/app/models/project_repository_storage_move.rb
# state :initial, value: 1
# state :scheduled, value: 2
# state :started, value: 3
# state :finished, value: 4
# state :failed, value: 5
# state :replicated, value: 6
# state :cleanup_failed, value: 7
user_project.repository_storage_moves.find(storage_move.id).state

# start housekeeping
jid = Projects::HousekeepingService.new(user_project, :gc).execute

# monitor if the job is running
workers = Sidekiq::Workers.new
workers.each do |process_id, thread_id, work| ; if work["payload"]["jid"] == jid then puts "running"; end ; end

# go on to the next one
```

## function to move a project shard

```ruby
def wait_for_job_to_finish(jid)
  running = true
  workers = Sidekiq::Workers.new

  while running do
    sleep(2)
    running = false
    workers.each do |process_id, thread_id, work|
      if work["payload"]["jid"] == jid then
        running = true
      end
    end
  end
end

def set_pool_repository(user_project)
  # refresh the project
  user_project = Project.find(user_project.id)

  # if the fork source belongs to a pool on the same shard
  if (user_project.fork_source.repository_storage == user_project.repository_storage and
      user_project.fork_source.pool_repository != nil)
    user_project.pool_repository = user_project.fork_source.pool_repository
    jid = ObjectPool::JoinWorker.perform_async(user_project.fork_source.id, user_project.id)
    user_project.save!
    wait_for_job_to_finish(jid)
  end
end

def run_housekeeping_sync(user_project)
  # start housekeeping
  begin
    jid = Repositories::HousekeepingService.new(user_project, :gc).execute

    wait_for_job_to_finish(jid)
  rescue ::Repositories::HousekeepingService::LeaseTaken => error
    puts error.message
    return false
  end
  return true
end


def move_project_shard_sync(user_project, dest)
  # refresh the project
  user_project = Project.find(user_project.id)

  if user_project.repository_storage == dest then
    puts 'source (' + user_project.repository_storage + ') equals dest (' + dest + ')'
    return false
  end

  # usual greetings
  puts "will move " + user_project.full_path + " from '" + user_project.repository_storage + "' to '" + dest + "'"

  # initiate transfer
  storage_move = user_project.repository_storage_moves.build({:source_storage_name=>user_project.repository_storage, :destination_storage_name=>dest})

  if not storage_move.schedule then
    puts 'cannot move ' + user_project.full_path
    return false
  end

  # wait for the transfere to complete
  while user_project.repository_storage_moves.find(storage_move.id).state < 4 do
    sleep(2)
  end

  if user_project.repository_storage_moves.find(storage_move.id).state == 5 # failed
    return false
  end

  if user_project.fork_source != nil then
    # if the project is a fork and the source belongs to a pool on the same shard, set back deduplication
    set_pool_repository(user_project)
  elsif user_project.fork_network != nil and user_project.fork_network.root_project != nil then
    # if the project is forked, rebuild a pool
    user_project = Project.find(user_project.id)
    user_project.object_pool_params
    running = true
    while running do
      sleep(2)
      user_project = Project.find(user_project.id)
      running = user_project.pool_repository.nil?
    end
  end

  # start housekeeping
  return run_housekeeping_sync(user_project)
end

def ensure_git_deduplication(user_project)
  # refresh the project
  user_project = Project.find(user_project.id)

  if user_project.fork_network != nil and user_project.fork_network.root_project_id == user_project.id
    # the repo is a source, do nothing
    return false
  end

  if user_project.fork_source.nil?
    puts user_project.full_path + " is not a fork"
    return false
  end

  if user_project.fork_source.pool_repository.nil?
    puts user_project.fork_source.full_path + " is not a pool source"
  end

  dest = user_project.fork_source.repository_storage

  if user_project.repository_storage != dest then
    return move_project_shard_sync(user_project, dest)
  end

  # same shard, ensure git deduplication is in place
  if user_project.pool_repository != user_project.fork_source.pool_repository
    puts "set git deduplication for " + user_project.full_path
    set_pool_repository(user_project)
    return run_housekeeping_sync(user_project)
  end

  return true
end

def move_project_and_forks(project, destination)
  source_project = Project.find(project.id)
  move_project_shard_sync(source_project, destination)
  source_project = Project.find(source_project.id)
  if source_project.fork_network.nil?
    return
  end

  Project.where(id: source_project.fork_network.fork_network_members.each().map { |f| f.project_id }.to_a).each() {
    |p| ensure_git_deduplication(p)
  }

end

project = Project.find_by_full_path('test_namespace/drm')
move_project_shard_sync(project, 'gitaly-1')
```

## Automate the thing for one source project

```ruby
source_project = Project.find_by_full_path('mesa/drm')

Project.where(id: source_project.fork_network.fork_network_members.each().map { |f| f.project_id }.to_a).where.not(repository_storage: source_project.repository_storage).each() { |p| move_project_shard_sync(p, source_project.repository_storage)}
```

## Move all sources of forks in a given group/namespace

```ruby
group = Group.find_by_path_or_name('lib')
Project.where(id: PoolRepository.all.map() {|pr| pr.source_project_id }.to_a,
        repository_storage: 'gitaly-1',
        namespace: group).each() {
  |project|
  move_project_and_forks(project, 'default')
}
```

## Even better, check all source of forks from a given shard

```ruby
Project.where(id: PoolRepository.all.map() { |pr| pr.source_project_id }.to_a, repository_storage: 'default').each() {
  |source_project|
  Project.where(id: source_project.fork_network.fork_network_members.each().map { |f| f.project_id }.to_a).where.not(repository_storage: source_project.repository_storage).each() {
    |p|
    move_project_shard_sync(p, source_project.repository_storage)
  }
}
```

## Last but not least, how to ensure git deduplication on the whole infra

### one specific project
```ruby
source_project = Project.find_by_full_path('Mesa_CI/mesa_perf')

# Ensure the given project has an associated pool_repository
source_project.object_pool_params

# then run the git dedup on all of its forks
# (this might move project between shards)
Project.where(id: source_project.fork_network.fork_network_members.each().map { |f| f.project_id }.to_a).each() {
  |p| ensure_git_deduplication(p)
}
```

### run git deduplication on all forks
```ruby

Project.where(id: PoolRepository.all.map() { |pr| pr.source_project_id }.to_a).each() {
  |source_project|
  if source_project.fork_network != nil then
    Project.where(id: source_project.fork_network.fork_network_members.each().map { |f| f.project_id }.to_a).each() {
      |p|
      ensure_git_deduplication(p)
    }
  end
}
```

# Snippets:

```ruby
Snippet.all.each() {
  |snip|
  if snip.repository_storage == "gitaly-1"
    storage_move = snip.repository_storage_moves.build({:source_storage_name=>snip.repository_storage, :destination_storage_name=>"default"})
    storage_move.schedule
  end }
```
